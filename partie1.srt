﻿1
00:00:08,444 --> 00:00:15,260
Le marché des télécoms avant sa régulation n'était pas un  marché, c'était un monopole.

2
00:00:15,260 --> 00:00:19,260
Il n'y avait d'opérateur télécoms en France que France Télécom.

3
00:00:19,420 --> 00:00:23,900
France Télécom avait des revendeurs et quelques prestataires internes mais

4
00:00:23,900 --> 00:00:29,660
pour qu'un marché apparaisse il faut casser ce monopole.

5
00:00:29,660 --> 00:00:33,420
Ce monopole est brisé en 1997 par l'arrivée de l'ARCEP

6
00:00:33,420 --> 00:00:39,120
qui démantèle cette entreprise d'État en plusieurs entreprises privées ou semi-privées

7
00:00:39,120 --> 00:00:43,300
Un marché émerge alors,  on parle de marché quand on a au moins 2 acteurs.

8
00:00:43,300 --> 00:00:49,100
Aujourd'hui il y a 4 gros opérateurs qui fonctionnent bien et 2000 autour.

9
00:00:49,100 --> 00:00:50,790
On a donc une diversité

10
00:00:50,790 --> 00:00:56,520
avec des mastodontes, des moyens, des minuscules, des spécialisés et des généralistes.

11
00:00:56,520 --> 00:01:00,740
Cette diversité économique et technique est très intéressante et importante.

12
00:01:00,740 --> 00:01:04,320
C'est un marché qui se porte bien quand on a ce genre de structures-là.

13
00:01:04,320 --> 00:01:08,940
On a donc les quatre très grands et 2000 petits.

14
00:01:08,940 --> 00:01:16,130
Si on les laisse s'entre-dévorer, en 5 à 10 ans il y aura  un monopole ;

15
00:01:16,130 --> 00:01:19,190
qui devrait finir racheté par Google

16
00:01:19,250 --> 00:01:21,730
ou en partenariat commercial avec Google.

17
00:01:22,300 --> 00:01:27,300
Donc il y a besoin que le marché soit régulé, parce que le marché tout seul ne fonctionne pas.

18
00:01:27,300 --> 00:01:32,620
On pourrait croire que c'est spécifique aux télécoms, il se trouve qu'en pratique ce n'est pas spécifique :

19
00:01:32,620 --> 00:01:35,860
il y a des tas de domaines où, si on ne régule pas le marché, il fait n'importe quoi

20
00:01:35,860 --> 00:01:38,880
sur la qualité de ce qui est produit et sur son fonctionnement économique...

21
00:01:38,940 --> 00:01:40,610
et tout le monde est au courant.

22
00:01:40,610 --> 00:01:47,860
Même les institutions régulatrices de l'U.E. savent que ce marché n'est pas autonome.

23
00:01:47,860 --> 00:01:49,780
C'est pas commun.

24
00:01:49,780 --> 00:01:52,120
Ça c'est la première chose à comprendre.

25
00:01:52,120 --> 00:01:56,820
La deuxième chose à comprendre c'est : qu'est-ce que vient faire la neutralité du net à l’intérieur de ce sujet là ?

26
00:01:56,820 --> 00:02:01,120
Ça c'est le contenu des... je ne sais plus 3-4 chapitres qui viennent

27
00:02:01,120 --> 00:02:05,920
qui sont  : pourquoi NOUS on veut faire de la régulation, et non pas pourquoi EUX ils veulent en faire.

