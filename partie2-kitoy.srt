﻿1
00:00:08,013 --> 00:00:15,320
En 2014 le reglement sur l'Internet ouvert a été voté par le parlement européen

2
00:00:15,520 --> 00:00:21,820
qui statue notamment qu'est ce que doit être l'Internet pour respecter les libertés fondamentales.

3
00:00:22,320 --> 00:00:25,680
Ce réglement a donné pouvoir à tous les régulateurs européens

4
00:00:25,786 --> 00:00:29,620
Il y autant de régulateurs que d'états membres, réunis au sein du BEREC

5
00:00:29,820 --> 00:00:35,820
L'Union Européenne a donné pouvoir aux régulateurs de s'occuper du sujet de la neutralité du net

6
00:00:36,000 --> 00:00:39,573
Toi régulateur, qui est un régulateur sectoriel uniquement économique

7
00:00:39,850 --> 00:00:44,333
dont la seule arme était : jouer sur la la concurrence libre et non faussée.

8
00:00:44,413 --> 00:00:50,520
Tu vas t'occuper aussi de vérifier, de t'assurer, que la manière dont le réseau est fait,

9
00:00:50,640 --> 00:00:52,640
la manière dont les opérateurs sont organisés,

10
00:00:52,813 --> 00:00:54,733
ne casse pas les libertés fondamentales.

11
00:00:55,213 --> 00:00:58,820
Alors là ça devient presque compliqué à comprendre :

12
00:00:59,000 --> 00:01:03,820
Il reste dans la tête des régulateurs des télécoms en France et dans les 28 pays d'Europe

13
00:01:04,000 --> 00:01:09,520
cette idée que l'outil générique c'est : l'ouverture du marché.

14
00:01:09,820 --> 00:01:12,666
Or, quand l'outil générique c'est l'ouverture du marché,

15
00:01:12,780 --> 00:01:15,960
ils ont du mal à analyser les questions de neutralité du net 

16
00:01:15,960 --> 00:01:18,413
autrement que comme des questions économiques.

17
00:01:20,320 --> 00:01:25,053
La neutralité du net ; si on reprend les enjeux qui sont fondamentaux derrière les textes,

18
00:01:25,120 --> 00:01:26,213
il y a 2 grands volets : 

19
00:01:26,320 --> 00:01:28,066
Il y a les questions économiques :

20
00:01:28,146 --> 00:01:31,520
Permettre aux utilisateurs du réseau 

21
00:01:31,520 --> 00:01:39,820
que sont  Google, Facebook et toutes les plateformes de services, de vente, de contenus

22
00:01:40,000 --> 00:01:42,266
de se faire concurrence les unes les autres 

23
00:01:42,733 --> 00:01:46,320
et donc typiquement, quand Orange favorise Dailymotion

24
00:01:46,320 --> 00:01:50,560
cela se fait au détriment de YouTube, PeerTube, Vimeo, etc.

25
00:01:50,740 --> 00:01:52,826
et c'est une atteinte à la concurrence.

26
00:01:53,820 --> 00:01:58,520
Il y a un paquet de questions économiques de ce genre autour de la neutralité du net.

27
00:01:59,000 --> 00:02:03,000
Quand Free dit, à tel ou tel marchand de vidéos :

28
00:02:03,320 --> 00:02:07,466
Si tu veux que tes vidéos soient fluides sur le réseau vers les utilisateurs,

29
00:02:07,506 --> 00:02:09,860
 tu me donnes 5% de ton Chiffre d'Affaires ;

30
00:02:10,000 --> 00:02:12,173
on parle bien de questions économiques.

31
00:02:12,820 --> 00:02:15,453
Or, il y a une deuxième lecture de la neutralité du net, 

32
00:02:15,480 --> 00:02:20,800
qui est fondatrice de la Fédération FDN

33
00:02:21,053 --> 00:02:23,480
qui est la liberté du net 

34
00:02:23,570 --> 00:02:30,986
comme pillier d'éléments démocratiques et des libertés publiques en ligne.

35
00:02:31,000 --> 00:02:33,373
C'est ce qui protège la liberté d'expression 

36
00:02:33,480 --> 00:02:36,386
et la liberté d'accéder à l'information de ton choix.

37
00:02:36,520 --> 00:02:39,600
Parce que si ton opérateur a décidé 

38
00:02:39,600 --> 00:02:42,853
que tel ou tel site d'information ne l’intéresse pas

39
00:02:42,893 --> 00:02:45,453
et qu'il préfère un partenariat avec BFM et TF1, 

40
00:02:45,453 --> 00:02:47,450
tu ne verras que BFM et TF1.

41
00:02:48,640 --> 00:02:49,600
Ça c'est embêtant, 

42
00:02:49,760 --> 00:02:52,900
parce que ça déforme l'information à laquelle tu as accès.

43
00:02:53,000 --> 00:02:56,213
Parce qu'en interdisant certains usages

44
00:02:56,290 --> 00:02:58,866
on interdit certains modes de socialisation

45
00:02:59,320 --> 00:03:02,000
et c'est pas le rôle d'un opérateur de faire ça.

46
00:03:02,130 --> 00:03:03,986
Et ça c'est ce qui correspond 

47
00:03:04,160 --> 00:03:07,333
au volet libertés fondamentales de la neutralité du net

48
00:03:08,320 --> 00:03:11,240
et ce volet-là, il est confié aux régulateurs des télécoms.

49
00:03:11,320 --> 00:03:14,520
Et le régulateur des télécoms si on le laisse bosser tout seul,

50
00:03:14,680 --> 00:03:15,973
il ne va pas s'en sortir 

51
00:03:16,060 --> 00:03:18,640
parce que lui il ne va voir que le volet économique.

