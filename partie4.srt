﻿1
00:00:08,400 --> 00:00:10,520
La fibre optique c'est un excellent exemple

2
00:00:10,520 --> 00:00:15,140
du travail que fait la fédération autour des questions de régulation.

3
00:00:15,240 --> 00:00:17,800
La fibre optique est en train de se déployer en France.

4
00:00:17,940 --> 00:00:19,800
Globalement ça fonctionne.

5
00:00:19,940 --> 00:00:23,857
Il y a à peu près de l'accès fibre qui se développe plutôt dans les grandes villes

6
00:00:23,942 --> 00:00:25,800
et ça va venir dans les campagnes.

7
00:00:25,820 --> 00:00:29,542
Sur ces accès, il y a de la concurrence libre et non faussée

8
00:00:29,571 --> 00:00:34,820
entre Orange en monopole, Free un tout petit peu et puis SFR marginalement.

9
00:00:34,885 --> 00:00:38,857
Bouygues ... ils sont plutôt mort dans ce secteur là.

10
00:00:38,940 --> 00:00:42,720
Ils font peut-être des choses mais pas beaucoup.

11
00:00:42,857 --> 00:00:44,710
Ça, c'est pas bon !

12
00:00:44,740 --> 00:00:46,007
Ça veut dire que

13
00:00:46,050 --> 00:00:49,461
sur les 2000 opérateurs déclarés à l'ARCEP

14
00:00:49,600 --> 00:00:53,320
dont à peu près un millier sont capables de fournir un accès Internet,

15
00:00:53,428 --> 00:00:57,520
il y en a 4 qui fournissent de l'accès sur l'infrastructure fibre.

16
00:00:57,571 --> 00:01:01,000
C'est pas ça l'ouverture à la concurrence, c'est pas comme ça que ça marche.

17
00:01:01,000 --> 00:01:04,942
Sur l'ADSL, le réseau que vous utilisez majoritairement si c'est pas la fibre,

18
00:01:05,000 --> 00:01:07,970
- c'est du réseau qui passe par les câbles téléphonique -

19
00:01:08,000 --> 00:01:10,298
il y a une obligation qui est donnée, notamment à Orange,

20
00:01:10,392 --> 00:01:13,080
de relouer son réseau à ses petit camarades

21
00:01:13,110 --> 00:01:16,857
qui sont plus petit que lui et qui n'ont pas l'argent pour investir dans ce réseau.

22
00:01:16,857 --> 00:01:21,510
Ce serait débile de refaire tout un réseau téléphonique dans toute la France

23
00:01:21,570 --> 00:01:24,542
juste pour qu'un deuxième opérateur opère dessus.

24
00:01:24,628 --> 00:01:25,820
Ça parait hallucinant.

25
00:01:25,890 --> 00:01:26,923
C'est hallucinant.

26
00:01:27,010 --> 00:01:31,280
Du coup pour ne pas faire ça, on dit au gros qui a fait ça tout seul comme un grand

27
00:01:31,520 --> 00:01:33,920
a l'époque France Télécom, maintenant Orange. 

28
00:01:33,920 --> 00:01:35,149
On lui dit "écoute pépère,

29
00:01:35,220 --> 00:01:38,283
tu vas louer à tes petits camarades le réseau existant, 

30
00:01:38,340 --> 00:01:40,130
vous allez le partager, et tout ira bien :

31
00:01:40,181 --> 00:01:42,460
les petits pourront vendre leurs petites offres

32
00:01:42,560 --> 00:01:44,123
toi ça va pas t'empêcher de vivre

33
00:01:44,167 --> 00:01:46,830
parce que tu vas pas perdre des milliers de parts de marché,

34
00:01:46,836 --> 00:01:48,460
on parle d'un micro-opérateur.

35
00:01:48,460 --> 00:01:51,140
Comme ça tout le monde peut survivre, et l'écosystème est maintenu."

36
00:01:51,190 --> 00:01:52,814
Ça n'existe pas sur la fibre optique.

37
00:01:52,901 --> 00:01:54,000
 Sur la fibre optique,

38
00:01:54,116 --> 00:01:58,580
le réseau se construit principalement pour arriver sur un monopole

39
00:01:58,618 --> 00:02:00,520
ou un duopole au mieux.

40
00:02:00,520 --> 00:02:06,334
Mais il n'est pas construit, pour l'instant, pour avoir cette péréquation d'accès à l'infrastructure.

41
00:02:06,450 --> 00:02:10,800
Le régulateur essaye de corriger ça un petit peu depuis 2 ans,

42
00:02:10,900 --> 00:02:13,723
pour faire re-rentrer un certain nombre d'acteurs dedans.

43
00:02:13,832 --> 00:02:21,000
Pour le moment les acteurs de la taille et du mode de fonctionnement que sont les fédérés

44
00:02:21,000 --> 00:02:24,520
ne peuvent pas fournir d'accès fibre optique à leurs membres.

45
00:02:24,520 --> 00:02:26,820
Le marché de la fibre ne le permet pas,

46
00:02:27,000 --> 00:02:30,749
parce que les 4 opérateurs en situations de monopole 

47
00:02:30,880 --> 00:02:32,312
ont décidé de verrouiller le marché

48
00:02:32,380 --> 00:02:34,181
et le régulateur a décidé de laisser faire.

49
00:02:34,320 --> 00:02:38,000
Et donc on est en train de travailler, d'abord à documenter ce marché là

50
00:02:38,040 --> 00:02:40,334
pour regarder, territoire par territoire en France : 

51
00:02:40,385 --> 00:02:42,050
Comment fonctionne le réseau fibre ?

52
00:02:42,320 --> 00:02:44,660
Quel est l'opérateur qui le fait bouger ?

53
00:02:44,720 --> 00:02:47,149
Que ce soit un opérateur privé ou un opérateur public

54
00:02:47,140 --> 00:02:50,901
ou un opérateur privé en délégation du service public.

55
00:02:50,996 --> 00:02:53,000
 Oui ça peut être compliqué.

56
00:02:53,000 --> 00:02:56,450
Voir dans quelles conditions on peut, ou pas, accéder à ce réseau ?

57
00:02:56,450 --> 00:02:58,625
Est-ce qu'on peut, ou pas, le fournir à nos abonné·e·s ?

58
00:02:58,620 --> 00:03:03,320
Quelles sont les conditions réglementaires, juridiques, tarifaires pour le faire ?

59
00:03:03,520 --> 00:03:06,516
Et dans toutes les zones où ça ne nous plaît pas,

60
00:03:06,510 --> 00:03:09,556
c'est à dire a peu près 99% du territoire en France,

61
00:03:09,880 --> 00:03:13,400
voir quelles actions le régulateur devrait prendre là-dessus

62
00:03:13,520 --> 00:03:15,403
pour ensuite essayer de l'inciter,

63
00:03:15,510 --> 00:03:18,509
soit par du contentieux

64
00:03:18,500 --> 00:03:21,752
c'est à dire on va voir le gendarme des télécoms et ont dit :

65
00:03:21,820 --> 00:03:25,460
"Bonjour, moi c'est FDN. Je voudrais faire de la fibre. Orange il est méchant y veut pas.

66
00:03:25,549 --> 00:03:27,570
Soit on se lance des cocktails Molotov dans la tête, 

67
00:03:27,570 --> 00:03:30,712
soit toi gendarme tu trouves une solution pour arbitrer entre les deux.

68
00:03:30,880 --> 00:03:33,100
Normalement il cherche une solution entre les deux,

69
00:03:33,180 --> 00:03:35,592
 il nous répond pas que les cocktails Molotov c'est bien.

70
00:03:36,080 --> 00:03:37,716
Et donc le but c'est de chercher ça, 

71
00:03:37,710 --> 00:03:42,050
c'est de trouver comment on peut pousser le régulateur à agir

72
00:03:42,080 --> 00:03:45,520
même dans les endroits où il n'a pas forcement envie de se bouger.

