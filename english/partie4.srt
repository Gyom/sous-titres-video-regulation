﻿1
00:00:00,880 --> 00:00:03,724
The FDN Federation explains... telecom regulation

2
00:00:05,573 --> 00:00:08,400
Chapter four: optical fiber

3
00:00:08,640 --> 00:00:10,520
Optical fiber is an excellent example

4
00:00:10,520 --> 00:00:15,140
about the work done by our federation about regulation questions.

5
00:00:15,240 --> 00:00:17,800
Optical fiber is deploying in France.

6
00:00:17,940 --> 00:00:19,800
It mostly works.

7
00:00:19,940 --> 00:00:23,857
There are some fiber access that is being developed especially in big towns

8
00:00:23,942 --> 00:00:25,800
and it will come into the country side.

9
00:00:25,820 --> 00:00:29,542
On these accesses, there is free competition and not false one

10
00:00:29,571 --> 00:00:34,820
between Orange in a monopoly, Free a bit and SFR on the margin.

11
00:00:34,885 --> 00:00:38,857
Bouygues ... they are almost dead in this sector.

12
00:00:38,940 --> 00:00:42,720
They do perhaps something but not much

13
00:00:42,857 --> 00:00:44,710
This is not good!

14
00:00:44,740 --> 00:00:46,007
This means that

15
00:00:46,050 --> 00:00:49,461
on 2000 operators declared at ARCEP

16
00:00:49,600 --> 00:00:53,320
from which around a thousand are capable to provide Internet access

17
00:00:53,428 --> 00:00:57,520
there are 4 that provide access on the fiber infrastructure.

18
00:00:57,571 --> 00:01:01,000
This is not competition openess, this is not like that that it should work.

19
00:01:01,000 --> 00:01:04,942
On ASDL, the network that you use on the majority if it is not fiber

20
00:01:05,000 --> 00:01:07,970
- this is a network that go through telephone cable -

21
00:01:08,000 --> 00:01:10,298
Orange is under an obligation

22
00:01:10,392 --> 00:01:13,080
to rent its network to his mates

23
00:01:13,110 --> 00:01:16,857
who are smaller and do not have money to invest in this network.

24
00:01:16,857 --> 00:01:21,510
It would be stupid to redo a phone network in all France

25
00:01:21,570 --> 00:01:24,542
only to enable another operator to use it.

26
00:01:24,628 --> 00:01:25,820
That seems incredible.

27
00:01:25,890 --> 00:01:26,923
It is incredible.

28
00:01:27,010 --> 00:01:31,280
So in order to avoid that, we ask the big operator who has done that all by itself as a good guy

29
00:01:31,520 --> 00:01:33,920
called some time ago France Telecom, and now Orange.

30
00:01:33,920 --> 00:01:35,149
We ask it "listen grandad,

31
00:01:35,220 --> 00:01:38,283
you shall lend to your friendly competitor the existing network,

32
00:01:38,340 --> 00:01:40,130
you shall share it, and all will be good :

33
00:01:40,181 --> 00:01:42,460
smaller ones will be able to sell their small offers

34
00:01:42,560 --> 00:01:44,123
you that will not impact your life

35
00:01:44,167 --> 00:01:46,830
because you will not loose thousands of market share,

36
00:01:46,836 --> 00:01:48,460
we talk about a micro-operator.

37
00:01:48,460 --> 00:01:51,140
like that everyone can survive, and ecosystem is maintained."

38
00:01:51,190 --> 00:01:52,814
This does not exist on optical fiber.

39
00:01:52,901 --> 00:01:54,000
On optical fiber,

40
00:01:54,116 --> 00:01:58,580
the network build itself mainly to arrive to a monopoly

41
00:01:58,618 --> 00:02:00,520
or a duopoly at best.

42
00:02:00,520 --> 00:02:06,334
It is not built, for now, to have this infrastructure access equalization.

43
00:02:06,450 --> 00:02:10,800
Regulator tries to fix that for about 2 years,

44
00:02:10,900 --> 00:02:13,723
to make enter back a certain amount of actors.

45
00:02:13,832 --> 00:02:21,000
For now actors about the size and the way of working who are part of the federation

46
00:02:21,000 --> 00:02:24,520
can not provide optical fiber access to their members.

47
00:02:24,520 --> 00:02:26,820
The fiber market does not allow it,

48
00:02:27,000 --> 00:02:30,749
because the 4 operators in monopoly situation

49
00:02:30,880 --> 00:02:32,312
decided to lock-in the market

50
00:02:32,380 --> 00:02:34,181
and the regulator let them do it.

51
00:02:34,320 --> 00:02:38,000
So we are working at, first document this very market

52
00:02:38,040 --> 00:02:40,334
to look, territory by territory in France:

53
00:02:40,385 --> 00:02:42,050
How is the fiber network working?

54
00:02:42,320 --> 00:02:44,660
Who is the operator that makes it move?

55
00:02:44,720 --> 00:02:47,149
It may be a private operator or a public operator

56
00:02:47,140 --> 00:02:50,901
or a private operator with a public service delegation.

57
00:02:50,996 --> 00:02:53,000
Yes that can be complicated.

58
00:02:53,000 --> 00:02:56,450
To see in which conditions we can, or not, access to this network?

59
00:02:56,450 --> 00:02:58,625
Can we, or not, provide it to our users?

60
00:02:58,620 --> 00:03:03,320
What are the reglementary, legal, fare, conditions to do it?

61
00:03:03,520 --> 00:03:06,516
And in all area where it does not suit us,

62
00:03:06,510 --> 00:03:09,556
this means around 99% of France territory,

63
00:03:09,880 --> 00:03:13,400
see what actions the regulator should take on that

64
00:03:13,520 --> 00:03:15,403
in order to try to incitate it,

65
00:03:15,510 --> 00:03:18,509
either by litigation

66
00:03:18,500 --> 00:03:21,752
this means, we go to the telecom gendarme and we say:

67
00:03:21,820 --> 00:03:25,460
"Hello, I am FDN. I would like to make fiber. Orange is naughty, it does not want.

68
00:03:25,549 --> 00:03:27,570
So either we throw Molotov cocktails to each other head,

69
00:03:27,570 --> 00:03:30,712
or, you gendarme you find a solution to arbitrate between us.

70
00:03:30,880 --> 00:03:33,100
Usually it search a solution between both,

71
00:03:33,180 --> 00:03:35,592
it answers that Molotov cocktails is not good.

72
00:03:36,080 --> 00:03:37,716
And so the goal is to look for that,

73
00:03:37,710 --> 00:03:42,050
find how we can push regulator to act

74
00:03:42,080 --> 00:03:45,520
even in areas where it does not want to move.

