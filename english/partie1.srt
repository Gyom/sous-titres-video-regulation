﻿1
00:00:00,240 --> 00:00:03,511
The FDN Federation explains... telecom regulation

2
00:00:03,653 --> 00:00:07,937
Chapter one: Why is there a telecom regulation?

3
00:00:08,693 --> 00:00:15,260
Before its regulation, telecom market was a monopoly.

4
00:00:15,260 --> 00:00:19,260
There was no other telecom operator than France Telecom

5
00:00:19,420 --> 00:00:23,900
France Telecom had resellers and some internal service providers but

6
00:00:23,900 --> 00:00:29,660
in order to create a market you have to breakout this monopoly.

7
00:00:29,660 --> 00:00:33,420
This monopoly was breakout in 1997 with the creation of ARCEP (France Telecommunication Regulator Agency)

8
00:00:33,420 --> 00:00:39,120
which dismantled this state own enterprise into several private or semi-private enterprises.

9
00:00:39,120 --> 00:00:43,300
A market was then able to emerge, we can speak of market when we have at least 2 actors.

10
00:00:43,300 --> 00:00:49,100
Today there are 4 big operators going well and 2000 around them.

11
00:00:49,100 --> 00:00:50,790
We so have a diversity

12
00:00:50,790 --> 00:00:56,520
with some giants, some middle sized, some tiny, some specialized and some general.

13
00:00:56,520 --> 00:01:00,740
This economical and technical diversity is very interesting and important.

14
00:01:00,740 --> 00:01:04,320
This is a market that behave correctly when we have this kind of entities.

15
00:01:04,320 --> 00:01:08,940
So we have 4 very big, and 2000 small.

16
00:01:08,940 --> 00:01:16,130
If we let them eat each other, in 5 to 10 years there will be a monopoly;

17
00:01:16,130 --> 00:01:19,190
which will in final being bought by Google

18
00:01:19,250 --> 00:01:21,730
or in commercial partnership with Google.

19
00:01:22,300 --> 00:01:27,300
So there is a need for the market to be regulated

20
00:01:27,300 --> 00:01:32,620
We may think that this is Telecom specific, but in pratical this is not:

21
00:01:32,620 --> 00:01:35,860
there are lot of domains where, if you do not regulate the market, it do silly things

22
00:01:35,860 --> 00:01:38,880
on the product quality and on its economic functioning...

23
00:01:38,940 --> 00:01:40,610
and everyone is aware.

24
00:01:40,610 --> 00:01:47,860
Even E.U. regulatory organization know that this market is not autonomous.

25
00:01:47,860 --> 00:01:49,780
This is strange.

26
00:01:49,780 --> 00:01:52,120
This is the first thing to understand.

27
00:01:52,120 --> 00:01:56,820
The seconde one is: what is doing internet neutrality in this very topic?

28
00:01:56,820 --> 00:02:01,120
This is the content of... I don't know 3-4 chapter that are coming

29
00:02:01,120 --> 00:02:05,920
which are: Why do WE want to do regulation, and not why THEY want to do it.

