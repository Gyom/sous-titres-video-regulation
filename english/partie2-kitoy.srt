﻿1
00:00:00,977 --> 00:00:04,675
The FDN Federation explains... telecom regulation

2
00:00:04,826 --> 00:00:07,484
Chapter two: Internet neutrality

3
00:00:08,013 --> 00:00:15,320
In 2014 an Internet regulation was voted by European Parliament

4
00:00:15,520 --> 00:00:21,820
that state in particular what should be Internet to respect fundamental liberty rights.

5
00:00:22,320 --> 00:00:25,680
This regulation act granted rights to all european regulators

6
00:00:25,786 --> 00:00:29,620
There are as many regulator than state members, gathered at BEREC

7
00:00:29,820 --> 00:00:35,820
Europe Union granted rights to regulators to manage Internet neutrality

8
00:00:36,000 --> 00:00:39,573
You regulator, who is a sectorial only economical regulator

9
00:00:39,850 --> 00:00:44,333
whom the only weapon was: play on free and not distorted concurrence

10
00:00:44,413 --> 00:00:50,520
You will manage also the checking, to insure that the way the network is done

11
00:00:50,640 --> 00:00:52,640
the way operators are organized

12
00:00:52,813 --> 00:00:54,733
does not break fundamental liberty rights.

13
00:00:55,213 --> 00:00:58,820
Then it become quite difficult to understand:

14
00:00:59,000 --> 00:01:03,820
There is still in the telecom regulators head in France and in the 28 European countries

15
00:01:04,000 --> 00:01:09,520
this idea that the generic tool to use is : market openess.

16
00:01:09,820 --> 00:01:12,666
However, when the generic tool is market openess,

17
00:01:12,780 --> 00:01:15,960
they have difficulties to analyse Internet neutrality questions

18
00:01:15,960 --> 00:01:18,413
differently from economical questions.

19
00:01:20,320 --> 00:01:25,053
Internet neutrality; If we take back fundamental stakes behing law rules,

20
00:01:25,120 --> 00:01:26,213
there are 2 folds :

21
00:01:26,320 --> 00:01:28,066
There are economical questions :

22
00:01:28,146 --> 00:01:31,520
Allow to network users

23
00:01:31,520 --> 00:01:39,820
that are Google, Facebook and all other service, sell, content plateforms,

24
00:01:40,000 --> 00:01:42,266
to be eachother in competition

25
00:01:42,733 --> 00:01:46,320
and so typically, when Orange is favoring Dailymotion

26
00:01:46,320 --> 00:01:50,560
this is to the detriment of YouTube, PeerTube, Vimeo, etc.

27
00:01:50,740 --> 00:01:52,826
and this is breach to fair competition.

28
00:01:53,820 --> 00:01:58,520
There is a lot of economical question of this kind around Internet neutrality.

29
00:01:59,000 --> 00:02:03,000
When Free say, to one or another video seller :

30
00:02:03,320 --> 00:02:07,466
If you want your videos to play smooth on the network toward the users,

31
00:02:07,506 --> 00:02:09,860
you give me 5% of your turnover ;

32
00:02:10,000 --> 00:02:12,173
we are on economical questions.

33
00:02:12,820 --> 00:02:15,453
However, there is a second way to read Internet neutrality,

34
00:02:15,480 --> 00:02:20,800
that is the fondation of FDN federation

35
00:02:21,053 --> 00:02:23,480
which is the Internet freedom

36
00:02:23,570 --> 00:02:30,986
as pilars to democratic elements and public liberty rights online.

37
00:02:31,000 --> 00:02:33,373
This is what protects freedom of expression

38
00:02:33,480 --> 00:02:36,386
and the freedom to access information of your choice.

39
00:02:36,520 --> 00:02:39,600
Because if your operator decided

40
00:02:39,600 --> 00:02:42,853
that this or this news website does not interest it

41
00:02:42,893 --> 00:02:45,453
and that it prefers a partnership with BFM and TF1,

42
00:02:45,453 --> 00:02:47,450
you will see BFM and TF1 only.

43
00:02:48,640 --> 00:02:49,600
This is annoying,

44
00:02:49,760 --> 00:02:52,900
because it distorts information to which you have access.

45
00:02:53,000 --> 00:02:56,213
By forbidding some uses

46
00:02:56,290 --> 00:02:58,866
we forbid some socialization maners

47
00:02:59,320 --> 00:03:02,000
and this is not the role of an operator to do that.

48
00:03:02,130 --> 00:03:03,986
And this is what belongs

49
00:03:04,160 --> 00:03:07,333
to the fundamental liberty rights fold of Internet neutrality

50
00:03:08,320 --> 00:03:11,240
and this fold, it is entrust to telecom regulators.

51
00:03:11,320 --> 00:03:14,520
And if we let the telecom regulator work alone,

52
00:03:14,680 --> 00:03:15,973
it won't manage to do it

53
00:03:16,060 --> 00:03:18,640
because it will only see the economic fold.

