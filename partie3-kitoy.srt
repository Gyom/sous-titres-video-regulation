﻿1
00:00:08,820 --> 00:00:11,320
Alors ça, c'est un truc simple à comprendre.

2
00:00:11,520 --> 00:00:17,280
Le régulateur fixe quelques règles du jeu qui s'appliquent

3
00:00:17,440 --> 00:00:27,820
à des organismes de bienfaisance et des PME que sont : Orange, SFR, Free, Bouygues, Vinci.

4
00:00:28,000 --> 00:00:31,200
L'ARCEP a obligation de consulter les acteurs économiques.

5
00:00:31,220 --> 00:00:37,228
Lorsque l'ARCEP fait ça, elle va consulter des acteurs experts, mais sur une branche très réduite de marché.

6
00:00:37,820 --> 00:00:41,142
Donc elle va interroger un opérateur

7
00:00:41,170 --> 00:00:44,114
spécialisé dans l'Internet pour les avocats,

8
00:00:44,257 --> 00:00:49,170
qui va leur expliquer comment son business va mourir si sur cette frange là du marché

9
00:00:49,200 --> 00:00:51,120
on touche à tel ou tel paramètre.

10
00:00:51,120 --> 00:00:53,511
C'est des gens qui sont experts

11
00:00:53,550 --> 00:00:55,617
mais qui ont un angle de vue qui est minuscule.

12
00:00:55,680 --> 00:00:57,680
Ils ne regardent que leur segment du marché.

13
00:00:58,000 --> 00:01:00,204
En grande partie parce que ce qui les intéresse,

14
00:01:00,204 --> 00:01:01,571
c'est de sauver leur business.

15
00:01:01,570 --> 00:01:05,626
On sur un marché économique capitaliste rangé correctement.

16
00:01:05,620 --> 00:01:08,835
Les gens veulent gagner du pognon. C'est... comme ça.

17
00:01:09,128 --> 00:01:10,740
Et nous on arrive sur ce marché,

18
00:01:10,800 --> 00:01:14,080
en étant opérateur, en participant au jeu, en jouant comme les autres

19
00:01:14,133 --> 00:01:17,810
mais avec une vision du monde, qui n'est pas "il faut gagner du pognon",

20
00:01:17,848 --> 00:01:19,840
On s'en fiche vraiment de gagner du pognon.

21
00:01:20,200 --> 00:01:25,257
Si l'abonnement, on doit le vendre à 10€, à 50€ à 100€,

22
00:01:25,520 --> 00:01:27,200
on le vendra à 10, 20, 50 euros.

23
00:01:27,200 --> 00:01:31,440
A 50€ on aura plus de mal d'avoir des abonnés qu'a 10 forcément,

24
00:01:31,820 --> 00:01:33,937
mais ce n'est pas tellement le problème.

25
00:01:34,080 --> 00:01:35,930
On est des associations de bénévoles.

26
00:01:36,000 --> 00:01:38,371
Notre but premier c'est pas de gagner de l'argent

27
00:01:38,371 --> 00:01:41,250
pour pouvoir grossir et nourrir des actionnaires.

28
00:01:41,250 --> 00:01:41,955
c'est pas le sujet.

29
00:01:42,026 --> 00:01:45,320
Le sujet de nos associations, c'est de défendre la neutralité du net

30
00:01:45,450 --> 00:01:47,671
et de promouvoir la décentralisation des réseaux. 

31
00:01:47,742 --> 00:01:48,520
C'est pas pareil.

32
00:01:48,660 --> 00:01:50,542
Du coup, ça fait arriver 

33
00:01:50,540 --> 00:01:53,431
sur le marché, et dans la discussion avec le régulateur

34
00:01:53,573 --> 00:01:56,770
des acteurs qui n'ont pas une vue étroite mais une vue large.

35
00:01:57,000 --> 00:01:59,571
Ça fait arriver dans le bureau du régulateur

36
00:01:59,620 --> 00:02:01,942
des gens comme moi qui on fait des études de philosophie,

37
00:02:02,000 --> 00:02:03,591
qui viennent leur expliquer la philo...

38
00:02:03,662 --> 00:02:05,450
pour parler de télécoms.

39
00:02:05,450 --> 00:02:06,826
Avec cet angle de vue là,

40
00:02:06,960 --> 00:02:09,866
avec cette manière de cabler les choses très inattendue,

41
00:02:10,140 --> 00:02:12,257
on arrive a un angle d'analyse qui est plus large,

42
00:02:12,285 --> 00:02:15,280
et on arrive à toucher des problèmes de société

43
00:02:15,367 --> 00:02:16,420
que l'ARCEP voit pas 

44
00:02:16,426 --> 00:02:18,000
parce qu'elle n'a pas les bons outils.

45
00:02:18,000 --> 00:02:20,657
Quand les acteurs économique se font la guerre entre eux,

46
00:02:20,906 --> 00:02:23,457
quels dommages collatéraux il y a pour les gens ?

47
00:02:23,600 --> 00:02:25,980
Quels dommages collatéraux il y a sur la société ?

48
00:02:26,180 --> 00:02:28,320
En quoi est-ce que ça déforme la société ?

49
00:02:28,320 --> 00:02:31,000
Google qui se bastonne contre Facebook

50
00:02:31,200 --> 00:02:35,320
pour savoir qui aura le plus de parts de marché dans le marché publicitaire,

51
00:02:35,320 --> 00:02:36,942
c'est une question économique,

52
00:02:36,971 --> 00:02:40,742
et cela a des conséquences colossales sur la vie privée des gens,

53
00:02:40,840 --> 00:02:43,020
sur la façon dont la société se structure

54
00:02:43,320 --> 00:02:47,520
sur la façon dont les haines montent ou ne montent pas à l’intérieur des sociétés.

55
00:02:47,820 --> 00:02:50,520
C'est des conséquences extrêmement lourdes.

56
00:02:50,820 --> 00:02:52,981
On a une place très précise sur ce marché. 

57
00:02:53,076 --> 00:02:54,980
On est les seuls à avoir les deux pieds :

58
00:02:55,020 --> 00:02:58,600
On a un pied dans le marché, parce qu'on est opérateur on a cette activité-là.

59
00:02:58,650 --> 00:03:02,000
On est experts sur la question de "qu'est-ce qu'un opérateur" ?

60
00:03:02,057 --> 00:03:04,970
On a des opérateurs qui datent d'avant qu'Orange arrive.

61
00:03:05,000 --> 00:03:07,314
On a FDN c'est 1992.

62
00:03:07,340 --> 00:03:09,714
Et on a un pied complètement en dehors

63
00:03:09,740 --> 00:03:12,000
parce qu'on est des gens de la "société civile".

64
00:03:12,050 --> 00:03:12,906
On est bénévoles. 

65
00:03:12,960 --> 00:03:16,906
On a tous un autre métier à coté du fait d'être opérateur,

66
00:03:17,310 --> 00:03:19,484
et pour certains l'autre métier n'a rien à voir ;

67
00:03:19,590 --> 00:03:21,537
ça produit une ouverture d'esprit, 

68
00:03:21,640 --> 00:03:24,400
une largesse de vue qui est très intéressante et importante

69
00:03:24,880 --> 00:03:26,385
qui permet d'aider le régulateur

70
00:03:26,458 --> 00:03:30,785
à assumer cette mission de défense de la neutralité du net 

71
00:03:30,850 --> 00:03:32,400
pour laquelle elle n'est pas armé.

72
00:03:32,400 --> 00:03:34,640
On est un peu le bras armé du régulateur, mais pas rétribués.

73
00:03:34,680 --> 00:03:38,440
En partie parce qu'on a cette vision d’intérêt général que même l'ARCEP n'a pas.

74
00:03:38,514 --> 00:03:40,420
L'ARCEP n'a pas de vision d'intérêt général.

75
00:03:40,465 --> 00:03:43,770
Nous si, car on part d'abord de l'utilisateur final pour arriver sur le marché,

76
00:03:43,820 --> 00:03:47,342
alors que l'ARCEP part du marché pour arriver peut-être à l'utilisateur final.

77
00:03:47,342 --> 00:03:51,200
On prouve souvent qu'il y a un des mouvement qui marche mieux que l'autre.

